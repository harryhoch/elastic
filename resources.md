# Resources for learning about elastic search

* [Elastic search reference](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)

* [A practical guide on Elasticsearch Scoring and Relevancy](https://qbox.io/blog/practical-guide-elasticsearch-scoring-relevancy)

* [Introduction to Document Similarity with Elasticsearch](https://rebeccabilbro.github.io/intro-doc-similarity-with-elasticsearch/)

* [Find Similar Documents in Elasticsearch with “More Like This Query”](https://qbox.io/blog/mlt-similar-documents-in-elasticsearch-more-like-this-query)

* [A Practical Guide on Elasticsearch Scoring and Relevance](https://qbox.io/blog/practical-guide-elasticsearch-scoring-relevancy)

* [A Deep Dive into Significant Terms and Significant Text Bucket Aggregations in Elasticsearch](https://qbox.io/blog/a-deep-dive-into-significant-terms-and-significant-text-bucket-aggregations-in-elasticsearch)


* [Build a Search Engine with Node.js and Elasticsearch](https://www.sitepoint.com/search-engine-node-elasticsearch/)

* [Elasticsearch-js github repository](https://github.com/elastic/elasticsearch-js)

* [How to Integrate Elasticsearch into Your Node.js Application](https://qbox.io/blog/integrating-elasticsearch-into-node-js-application)

