// ingest.js
// read files in from a repository and index them.
// run with 'npm run ingest'
// or 'npm run --silent ingest'
//harryh@pitt.edu 201905161706


// set up var with directory to read
var docdir ="/Users/harry/Documents/research/projects/deeppheno/dev/v2-3.5/reports/single/";

// set up index name
var indexname= "deepphe"

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })


// assume we have names of the form "patientXX_all.txt"
function getId(file) {
    var b='patient';
    var start = b.length

    var underscore=file.indexOf('_')
    var numlength = underscore-b.length;
    return file.substr(start,numlength);
}


function processFile(file) {

    var fullpath = docdir+file;

    // pull out id
    id = getId(file);
    console.log(id);
    
    // read file
    
    var contents = fs.readFileSync(fullpath, 'utf8');
    indexContents(id,contents);
}


function indexContents(id,contents) {
    // create javascript structure

    var input = {
	id: id,
	body: contents
    };

    request = {
	index: indexname,
	id: id,
	body : input
    };
    client.index(request, function(err,resp,status) {
	console.log("patient " +id+" indexed "+JSON.stringify(resp));
    });
}




// go to directory
var fs=require('fs');
const args = require('yargs').argv;

console.log(process.argv)
console.log(args.dir);
if (args.dir) {
    console.log("setting dir..");
    docdir=args.dir;
}

if (args.index) {
    indexname=args.index;
}

if (docdir.slice(-1) !='/') {
    docdir = docdir+"/";
}

console.log("processing... "+docdir);
console.log("to index..."+indexname);

fs.readdir(docdir,function(err,items) {
    // list over files

    for (var i = 0; i <items.length; i++ ) {
	processFile(items[i]);
    }
});

// for each file, process it - read in and post..



