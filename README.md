
# Introduction


Some test code for using elasticsearch and Nodejs together. A [list of links to related resources](resources.md) is also available. 


# Calls to use for elasticsearch/elastic JS wrangling.


## Create index.


```
curl -X PUT "http://localhost:9200/deepphe" -H "Content-type: application/json" --data @deeppheindex.json
```

## Index an item


```
curl -X POST "localhost:9200/deepphe/_doc/3?pretty" -H 'Content-Type: application/json' -d@deepphetest.json
```

or, in [elasticsearch-js](https://github.com/elastic/elasticsearch-js):

```
request = {
	index: "deepphe",
	id: id,
	body : ....
};


client.index(request, function(err,resp,status) {
       console.log("patient " +id+" indexed");
});

```

where `body` is the payload that might be sent to elasticsearch via a rest call. 

---
## list indices


```
curl "localhost:9200/_cat/indices?v"
```

---
# search for all records

```
curl -X GET "localhost:9200/deepphe/_search?pretty" -H 'Content-Type: application/json' -d'{ "query": { "bool": { "must" : {"match_all" : {} }}}}'
```
----
# delete index

```
curl -X DELETE "localhost:9200/lib?pretty
```


---
# Simple term text
```
curl -X GET 'localhost:9200/deepphe/_search?pretty' -H "Content-Type: application/json"  -d '{"query": {"query_string" : {  "query" : "fantastic"} }    }'
```

=----

# explain query results for a document 

```
harryh-mdt:test harry$ curl -X POST 'localhost:9200/dptest/_explain/02?pretty' -H "Content-Type: application/json"  -d '{"query": {"query_string" : {  "query" : "bladder"} }    }'
```